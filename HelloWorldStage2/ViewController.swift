//
//  ViewController.swift
//  HelloWorldStage2
//
//  Created by chameleon on 06.07.17.
//  Copyright © 2017 chameleon. All rights reserved.
//

import UIKit
import Alamofire
import RealmSwift

class Message: Object {
    dynamic var text = ""
}

class ViewController: UIViewController {
    //Outlets
    @IBOutlet weak var outputLabel: UILabel!
    @IBOutlet weak var infoLabel: UILabel!
    
    // Init realm
    let realm = try! Realm()

    
    override func viewDidLoad() {
        super.viewDidLoad()
    }
    
    //Get json from the server
    @IBAction func getInfo(_ sender: Any) {
        let message = Message()
        // Request to the server
        Alamofire.request("http://10.15.14.119/hello", encoding: JSONEncoding.default).responseJSON {
            (response) in
            do {
                if let json_message = try JSONSerialization.jsonObject(with: response.data!) as? [String: AnyObject] {
                    message.text = json_message["message"] as? String ?? ""
                    try! self.realm.write {
                        self.realm.add(message)
                    }
                    self.infoLabel.text = "Success"
                }
            }
            catch {
                self.infoLabel.text = "Connection error"
            }
        }
        
        
    }
    
    @IBAction func writeInfo(_ sender: Any) {
        let message = self.realm.objects(Message.self).last
        self.outputLabel.text = message?.text
    }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
